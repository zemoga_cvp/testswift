# TestSwift

Mobile Tech Test - Version 1 
ZEMOGA Mobile Test 
Create an app that lists all messages and their details from JSONPlaceholder. Requirements: 

- [] [ProjectStruct:]
	CoreData
		- CoreConnection: Core to manage the REST conection 

		- CoreConnection.playground: Module for testing specific codes

		- CorePostLocalPersistence: Manage CRUD to Post in local persistence
		- CorePostCommentsLocalPersistence: Manage CRUD to PostComments in local persistence
		- CoreUserPostLocalPersistence: Manage CRUD to UserPost in local persistence
		- CoreControlLocalPersistence: Manage CRUD to Control to save in local persistence
		
	Models
		- Post: Model to Post and Comments integrate to Rest service in JSONPlaceholder
		- UserPost: Model to UserPost integrate to Rest service in JSONPlaceholder
		
		- ControlLocalPersistence: Model to local persistence for verfy the save in local
		- UserPostLocalPersistence: UserPost Model to local persistence
		- PostCommentsLocalPersistence: PostComments Model to local persistence
		- PostLocalPersistence: Post Model to local persistence
		
	ViewModels
		- PostViewModel: Manage comunication into View and Model from Post VIEW
		- PostDetailsViewModel: Manage comunication into View and Model from PostDetails VIEW
		- PostTableViewDataSource: Manage comunication into View and Model from Post table
		- PostCommentsTableViewDataSource: Manage comunication into View and Model from PostComments table
	
	ViewControllers
		- PostsViewControllers: Manage the view Posts, where the post list is displayed
		- PostDetailsViewControllers: Manage the view PostDetails, where the post details is displayed
	
	TableViewCell
		- PostTableViewCell: Handle the style of the List Post Cell
		- PostCommentsTableViewCell: Handle the style of the List PostComments cell
	
	Protocols
		- PostsActionView: Protocol for communication between controllers
		- PostsViewActionDelegate: 
		
	Common
		- AppDelegate
		- SceneDelegate

I use the MVVM architecture is used to try to show a small part of the benefit of this architecture 
by separating the Model from the view and the ViewModel, also creating a core for communication 
with the services, to try to have low coupling and high cohesion.


- [] [Notes:]
I tried to manage the persistence with Realm but due to time issues I couldn't finish it, 
the idea was that with ControlLocalPersistence it would be validated whether or not there 
was a local save and thus keep track of the saved data, perhaps placing dates and then 
synchronizing

-[] [Install Realm]
1. Add Package Dependency
   In Xcode, select File > Add Packages....

2. Specify the Repository
   Copy and paste the following into the search/input box.

   https://github.com/realm/realm-swift.git

3. Specify Options
  In the options for the realm-swift package, we recommend setting the Dependency Rule to 
  Up to Next Major Version, and enter the 
  current Realm Swift SDK version. Then, click Add Package.

4. Select the Package Products
  Select both Realm and RealmSwift, then click Add Package.