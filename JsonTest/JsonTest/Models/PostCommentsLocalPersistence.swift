//
//  PostCommentsLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import RealmSwift

class PostCommentsLocalPersistence: Object {
    @Persisted var id: Int = 0
    @Persisted var postID: Int = 0
    @Persisted var name = ""
    @Persisted var email = ""
    @Persisted var body: String = ""
    
    convenience init(id: Int, postID: Int, name: String, email: String, body: String) {
        self.init()
        
        self.id = id
        self.postID = postID
        self.name = name
        self.email = email
        self.body = body
    }
}
