//
//  Post.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 21/07/22.
//

import Foundation

typealias Posts = [PostInfo]
typealias PostComments = [PostComment]

// MARK: - PostInfo
struct PostInfo: Decodable {
    let userID, id: Int
    let title, body: String
    var favorite: Bool?

    enum CodingKeys: String, CodingKey {
        case userID = "userId"
        case id, title, body, favorite
    }
    
    mutating func changeFavorite(favorite: Bool) {
        self.favorite = favorite
    }
}

// MARK: - PostComment
struct PostComment: Decodable {
    let postID, id: Int
    let name, email, body: String

    enum CodingKeys: String, CodingKey {
        case postID = "postId"
        case id, name, email, body
    }
}
