//
//  ControlLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import RealmSwift

class ControlLocalPersistence: Object {
    @Persisted var id: Int = 0
    @Persisted var isSaveInLocal: Bool = false
    
    
    convenience init(id: Int, isSaveInLocal: Bool) {
        self.init()
        
        self.id = id
        self.isSaveInLocal = isSaveInLocal
    }
}
