//
//  PostLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import RealmSwift

class PostLocalPersistence: Object {
    @Persisted var id: Int = 0
    @Persisted var userID: Int = 0
    @Persisted var title = ""
    @Persisted var body: String = ""
    @Persisted var favorite: Bool?
    
    convenience init(id: Int, userID: Int, title: String, body: String, favorite: Bool) {
        self.init()
        
        self.id = id
        self.userID = userID
        self.title = title
        self.body = body
        self.favorite = favorite
    }
}
