//
//  UserPostLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import RealmSwift

class UserPostLocalPersistence: Object {
    @Persisted var id: Int = 0
    @Persisted var name = ""
    @Persisted var username = ""
    @Persisted var phone = ""
    @Persisted var website: String = ""
    @Persisted var company: String = ""
    
    
    convenience init(id: Int, name: String, username: String, phone: String, website: String, company: String) {
        self.init()
        
        self.id = id
        self.name = name
        self.username = username
        self.phone = phone
        self.website = website
        self.company = company
    }
}
