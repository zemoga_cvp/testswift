//
//  CoreControlLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import Foundation
import RealmSwift

public class CoreControlLocalPersistence {
    private let localRealm:Realm = try! Realm()
    
    // MARK: - CRUD CONTROL
    func addControl(controlLocalPersistence: ControlLocalPersistence) {
        try! localRealm.write {
            localRealm.add(controlLocalPersistence)
        }
    }
    
    func getAllControl() -> Results<ControlLocalPersistence> {
        let controls = localRealm.objects(ControlLocalPersistence.self)
        
        return controls;
    }
    
    func getControlById(controlId: Int) -> Results<ControlLocalPersistence> {
        let controls = self.getAllControl()
        
        let controlById = controls.where {
            $0.id == controlId
        }
        
        return controlById
    }
    
    func deleteControlById(controlId: Int) {
        let controls = self.getControlById(controlId: controlId)
        if(controls.count>0) {
            let controlToDelete = controls[0]
            try! localRealm.write {
                localRealm.delete(controlToDelete)
            }
        }
    }
}
