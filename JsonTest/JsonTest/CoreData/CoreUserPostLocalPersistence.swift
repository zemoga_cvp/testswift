//
//  CoreUserPostLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import Foundation
import RealmSwift

public class CoreUserPostLocalPersistence {
    private let localRealm:Realm = try! Realm()
    
    // MARK: - CRUD USER
    func addUserPost(userPostLocalPersistence: UserPostLocalPersistence) {
        try! localRealm.write {
            localRealm.add(userPostLocalPersistence)
        }
    }
    
    func getAllUserPosts() -> Results<UserPostLocalPersistence> {
        let userPosts = localRealm.objects(UserPostLocalPersistence.self)
        
        return userPosts;
    }
    
    func getUserPostById(postId: Int) -> Results<UserPostLocalPersistence> {
        let userPosts = self.getAllUserPosts()
        
        let postById = userPosts.where {
            $0.id == postId
        }
        
        return postById
    }
    
    func deleteUserPostById(userPostId: Int) {
        let userPosts = self.getUserPostById(postId: userPostId)
        let postToDelete = userPosts[0]
        try! localRealm.write {
            localRealm.delete(postToDelete)
        }
    }
}
