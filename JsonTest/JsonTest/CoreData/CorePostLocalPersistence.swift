//
//  CorePostLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import Foundation
import RealmSwift

public class CorePostLocalPersistence {
    private let localRealm:Realm = try! Realm()
    
    // MARK: - CRUD POST
    func addPost(postLocalPersistence: PostLocalPersistence) {
        try! localRealm.write {
            localRealm.add(postLocalPersistence)
        }
    }
    
    func getAllPosts() -> Results<PostLocalPersistence> {
        let posts = localRealm.objects(PostLocalPersistence.self)
        
        return posts;
    }
    
    func getPostById(postId: Int) -> Results<PostLocalPersistence> {
        let posts = self.getAllPosts()
        
        let postById = posts.where {
            $0.id == postId
        }
        
        return postById
    }
    
    func deletePostById(postId: Int) {
        let posts = self.getPostById(postId: postId)
        let postToDelete = posts[0]
        try! localRealm.write {
            localRealm.delete(postToDelete)
        }
    }
    
    // MARK: - convert betweens models
    func convertToPostLocalPersistence(postInfo: PostInfo) -> PostLocalPersistence{
        let postLocalPersistence = PostLocalPersistence(id: postInfo.id, userID: postInfo.userID, title: postInfo.title, body: postInfo.body, favorite: postInfo.favorite ?? false)
        
        return postLocalPersistence
    }
    
    func convertToPostsLocalPersistence(posts: Posts) -> [PostLocalPersistence] {
        var postsLocal: [PostLocalPersistence] = []
        
        for (_, postInfo) in posts.reversed().enumerated() {
            let postLocalPersistence = self.convertToPostLocalPersistence(postInfo: postInfo)
            postsLocal.append(postLocalPersistence)
        }
        
        return postsLocal
    }
}
