//
//  CorePostCommentsLocalPersistence.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import Foundation
import RealmSwift

public class CorePostCommentsLocalPersistence {
    private let localRealm:Realm = try! Realm()
    
    // MARK: - CRUD POST COMMENTS
    func addPostComments(postLocalPersistence: PostLocalPersistence) {
        try! localRealm.write {
            localRealm.add(postLocalPersistence)
        }
    }
    
    func getAllPostsCommets() -> Results<PostCommentsLocalPersistence> {
        let postsComments = localRealm.objects(PostCommentsLocalPersistence.self)
        
        return postsComments;
    }
    
    func getPostCommentsById(postId: Int) -> Results<PostCommentsLocalPersistence> {
        let postsComments = self.getAllPostsCommets()
        
        let postById = postsComments.where {
            $0.id == postId
        }
        
        return postById
    }
    
    func deletePostById(postId: Int) {
        let postsComments = self.getPostCommentsById(postId: postId)
        let postCommentsToDelete = postsComments[0]
        try! localRealm.write {
            localRealm.delete(postCommentsToDelete)
        }
    }
}
