import UIKit
class TestCore {
    let coreConnection = CoreConnection()

    func testGetListPostInfoFromUrl() {
        coreConnection.getListPostInfo { listPostInfo in
            print( "*** Init getListPostInfo ***" )
            for postInfo in listPostInfo {
                print("postInfo: \n id: \(postInfo.id) \n body: \(postInfo.body) \n title: \(postInfo.title) \n userID: \(postInfo.userID) \n\n")
        //        print( "Print List postInfo \(String(describing: listPostInfo))" )
            }
        }
    }

    func testDeletePostInfoFromUrl(postId: Int) {
        coreConnection.deletePostInfo(postId: postId, completion: { response in
            print( "*** Init deletePostInfo ***" )
            print( "Print postInfo: (\(postId)) - Commets: \(String(describing: response))" )
        })
    }

    func testGetDetailPostInfoFromUrl(postId: Int) {
        coreConnection.getDetailPostInfo(postId: postId, completion: { postInfo in
            print( "*** Init getDetailPostInfo ***" )
            print( "Print postInfo: (\(postId)) - Detail: \(String(describing: postInfo))" )
        })
    }

    func testGetListPostCommentFromUrl(postId: Int) {
        coreConnection.getListPostComment(postId: postId, completion: { listPostComments in
            print( "*** Init getListPostComment ***" )
            print( "Print postInfo: (\(postId)) - Commets: \(String(describing: listPostComments))" )
        })
    }


    func testGetUserPostInfoFromUrl(userPostId: Int) {
        coreConnection.getUserPostInfo(userPost: userPostId, completion: { userPost in
            print( "*** Init deletePostInfo ***" )
            print( "Print userPostId: (\(userPostId)) - userPost: \(String(describing: userPost))" )
        })
    }

    let coreControlLocalPersistence = CoreControlLocalPersistence()
    let corePostLocalPersistence = CorePostLocalPersistence()

    func testSaveInLocalPersistence() {
        print( "*** Init testSaveInLocalPersistence ***" )

        let controls = coreControlLocalPersistence.getAllControl()
        
        print( "Print controls: (\(controls))" )
        if(controls.count <= 0) {
            coreConnection.getListPostInfo { listPostInfo in
                print( "Print listPostInfo: (\(listPostInfo))" )

                let postLocal = self.corePostLocalPersistence.convertToPostsLocalPersistence(posts: listPostInfo)
                
                print( "Print postLocal: (\(postLocal))" )

                self.corePostLocalPersistence.addPost(postLocalPersistence: postLocal[0])
            }
            
            // We mark in the place that has already been saved
            print( "Print Make Local Control" )

            let control = ControlLocalPersistence()
            control.id = 1
            control.isSaveInLocal = true

            print( "Print control: (\(control))" )
            coreControlLocalPersistence.addControl(controlLocalPersistence: control)
        }
    }

    func testDeleteControl() {
        coreControlLocalPersistence.deleteControlById(controlId: 1)
        print( "Print control: delete control" )
    }
    
    func testSearchInLocal(postId: Int) {
        let postById = corePostLocalPersistence.getPostById(postId: postId)
        print(postById)
    }
}

let testCore = TestCore()
// testCore.testGetListPostInfoFromUrl(1)
// testCore.testDeletePostInfoFromUrl(1)
// testCore.testGetDetailPostInfoFromUrl(2)
// testCore.testGetListPostCommentFromUrl(2)
// testCore.testGetUserPostInfoFromUrl(2)
testCore.testDeleteControl()
testCore.testSaveInLocalPersistence()
testCore.testSearchInLocal(postId: 0)
