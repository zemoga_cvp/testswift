//
//  CoreConnection.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 22/07/22.
//

import Foundation

public class CoreConnection {
    
    private var urlConnection: String?
    private var params: [String: String]?
    private var nameMethod: String = "GET"
    
    public func setUrlConnection (newUrlConnection: String) {
        urlConnection = newUrlConnection
    }
    
    public func getUrlConnection () -> String {
        return urlConnection!
    }
    
    private func runConnection(completion : @escaping (Data) -> ()) {
        guard let theUrlConnection = urlConnection
        else{
            return;
        }
        
        
        let objURL = URL(string: theUrlConnection)
        var request = URLRequest(url: objURL!)
        request.httpMethod = nameMethod

        if(params != nil) {
            print("params: \(String(describing: params))")
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            //request.addValue("token", forHTTPHeaderField: "Authorization")
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: params!)
            } catch {
                print(error)
                return
            }
        }
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            if error != nil {
                print( "Found error when execute \(String(describing: error))" )
            } else {
                if let data = data {
                    print( "runConnection data: \(String(describing: data))" )
                    completion(data)
                }
            }
        }.resume()
    }
    
    func getListPostInfo(completion : @escaping (Posts) -> ()) {
        self.urlConnection = "https://jsonplaceholder.typicode.com/posts";
        
        self.runConnection { (data) in
            print( data )
            let jsonDecoder = JSONDecoder()
            let objJson = try! jsonDecoder.decode(Posts.self, from: data)
            completion(objJson)
        }
    }
    
    func getDetailPostInfo(postId: Int, completion : @escaping (PostInfo?) -> ()) {
        self.urlConnection = "https://jsonplaceholder.typicode.com/posts/\(postId)";
        
        self.runConnection { (data) in
            print( data )
            let jsonDecoder = JSONDecoder()
            let objJson = try! jsonDecoder.decode(PostInfo?.self, from: data)
            completion(objJson)
        }
    }
    
    func getUserPostInfo(userPost: Int, completion : @escaping (UserPost?) -> ()) {
        self.urlConnection = "https://jsonplaceholder.typicode.com/users/\(userPost)";
        
        self.runConnection { (data) in
            print( data )
            let jsonDecoder = JSONDecoder()
            let objJson = try! jsonDecoder.decode(UserPost?.self, from: data)
            completion(objJson)
        }
    }
    
    /**
     This service is a mockup, it is only implemented to show an example of its use
     */
    func deletePostInfo(postId: Int, completion : @escaping (Bool) -> ()) {
        self.urlConnection = "https://jsonplaceholder.typicode.com/posts/\(postId)";
        self.params = ["method": "DELETE"];
        self.nameMethod = "DELETE"
        self.runConnection { (data) in
            print( data )
            
            completion(true)
        }
    }
    
    func getListPostComment(postId: Int, completion : @escaping (PostComments) -> ()) {
        self.urlConnection = "https://jsonplaceholder.typicode.com/posts/\(postId)/comments";
        
        self.runConnection { (data) in
            print( data )
            let jsonDecoder = JSONDecoder()
            let objJson = try! jsonDecoder.decode(PostComments.self, from: data)
            completion(objJson)
        }
    }
}
