//
//  PostsViewActionDelegate.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import Foundation

protocol PostsViewActionDelegate {
    func handleUpdatePosts(postInfo: PostInfo, index: Int)
}

