//
//  PostsActionView.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import Foundation
import UIKit

class PostsActionView: UIView {
    var delegate: PostsViewActionDelegate?
    
    func updatePostsData(postInfo: PostInfo, index: Int) {
        guard let delegate = delegate else { return }
        
        delegate.handleUpdatePosts(postInfo: postInfo, index: index)
    }
}
