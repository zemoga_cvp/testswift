//
//  PostCommentsTableViewCell.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import UIKit

class PostCommentsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var postCommentsNameLabel: UILabel!
    @IBOutlet weak var postCommentsEmailLabel: UILabel!
    @IBOutlet weak var postCommentsDescLabel: UILabel!
    
    var postCommets : PostComment? {
        didSet {
            guard let name = postCommets?.name, let email = postCommets?.email, let desc = postCommets?.body
            else{
                return
            }
            
            postCommentsNameLabel.text  = "Name: \(String(describing: name))"
            postCommentsEmailLabel.text = "Email: \(String(describing: email))"
            postCommentsDescLabel.text  = "Desc: \(String(describing: desc))"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
