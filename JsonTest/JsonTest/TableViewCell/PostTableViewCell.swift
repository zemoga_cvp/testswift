//
//  PostTableViewCell.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 23/07/22.
//

import UIKit

class PostTableViewCell: UITableViewCell {
    
    @IBOutlet weak var postTitleLabel: UILabel!
    @IBOutlet weak var imgFavorito: UIImageView!
    
    var postInfo : PostInfo? {
        didSet {
            guard let postId = postInfo?.id, let postTitle = postInfo?.title
            else{
                return
            }
            
            postTitleLabel.text = "\(String(describing: postId)) - \(String(describing: postTitle))"

            let favorite: Bool = postInfo?.favorite ?? false;
            if(favorite){
                self.imgFavorito.image = UIImage(systemName: "star.fill")
            }else{
                self.imgFavorito.image = UIImage(systemName: "")
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
