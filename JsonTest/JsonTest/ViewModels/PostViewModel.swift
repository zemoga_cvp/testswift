//
//  PostViewModel.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 23/07/22.
//

import Foundation

class PostViewModel: NSObject {
    private var isFilterData : Bool = false
    private var coreService : CoreConnection!
    private(set) var originalPostsData : Posts!
    private(set) var postsData : Posts! {
        didSet {
            self.bindPostViewModelToController()
        }
    }
    
    var bindPostViewModelToController : (() -> ()) = {}
    
    override init() {
        super.init()
        self.coreService =  CoreConnection()
        callFuncToGetPostData()
    }
    
    func callFuncToGetPostData() {
        self.coreService.getListPostInfo { posts in
            self.postsData = posts
            self.originalPostsData = posts
        }
    }
    
    func callFuncToDeletePostData(at: Int) {
        self.deleteOriginalPostData(postId: self.postsData[at].id);
        self.postsData.remove(at: at)
    }
    
    func callFuncToUpdatePostData(at: Int, postInfo: PostInfo) {
        guard self.postsData.count >= at, self.postsData.count > 0
        else{
            return;
        }

        self.postsData[at] = postInfo
        self.updateOriginalPostData(atPost: at, postId: postInfo.id);
    }
    
    func callFuncToFilterPostData(filter: Bool) {
        guard self.postsData.count >= 0
        else{
            return;
        }
        if(filter) {
            let filterData = postsData.filter{ $0.favorite == true }
            self.postsData = filterData;
            self.isFilterData = true
        }else{
            self.postsData = self.originalPostsData
            self.isFilterData = false
        }
    }
    
    func callFuncToDeleteAllPostsData() {
        self.postsData.removeAll()
    }
    
    // MARK: - Search Methods
    private func updateOriginalPostData(atPost: Int, postId: Int) {
        if let row = self.originalPostsData.firstIndex(where: {$0.id == postId}) {
            self.originalPostsData[row] = self.postsData[atPost]
        }
    }
    
    private func deleteOriginalPostData(postId: Int) {
        if let row = self.originalPostsData.firstIndex(where: {$0.id == postId}) {
            self.originalPostsData.remove(at: row)
        }
    }
}
