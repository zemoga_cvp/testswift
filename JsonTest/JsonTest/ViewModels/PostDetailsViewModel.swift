//
//  PostDetailsViewModel.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 27/07/22.
//

import Foundation


class PostDetailsViewModel: NSObject {
    private var coreService : CoreConnection!
    private var selectedIndexPost: Int
    private(set) var postInfo : PostInfo! {
        didSet {
            self.bindPostDetailsViewModelToControllerOfPostInfo()
        }
    }
    private(set) var userPost : UserPost! {
        didSet {
            self.bindPostDetailsViewModelToControllerOfUserPost()
        }
    }
    
    private(set) var postComments : PostComments! {
        didSet {
            self.bindPostCommentsViewModelToControllerOfUserPost()
        }
    }

    var bindPostDetailsViewModelToControllerOfPostInfo : (() -> ()) = {}
    var bindPostDetailsViewModelToControllerOfUserPost : (() -> ()) = {}
    var bindPostCommentsViewModelToControllerOfUserPost: (() -> ()) = {}
    
    init(selectedIndexPost: Int, postSelected: PostInfo) {
        self.selectedIndexPost = selectedIndexPost
        
        super.init()
        self.coreService =  CoreConnection()
        self.callFuncToGetPostInfo(postSelected: postSelected)
    }
    
    func callFuncToGetPostInfo(postSelected: PostInfo) {
        DispatchQueue.main.async {
            self.postInfo = postSelected
            self.coreService.getUserPostInfo(userPost: self.postInfo!.userID) { userPost in
                guard let tmpUserPost =  userPost
                else{
                    return;
                }
                
                self.userPost = tmpUserPost
            }
            
            self.callFuncToGetPostComments(postSelected: self.postInfo)
        }
    }
    
    func callFuncToGetPostComments(postSelected: PostInfo) {
        self.coreService.getListPostComment(postId: self.postInfo!.userID) { postComments in
            self.postComments = postComments
        }
    }
    
    func setPostToFavorite(isFavorite: Bool){
        self.postInfo.favorite = isFavorite
    }
}

