//
//  PostsViewControllers.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 21/07/22.
//

import UIKit

class PostsViewControllers: UIViewController {
    @IBOutlet weak var postsTableView: UITableView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    private var postViewModel : PostViewModel!
    private var selectedIndexPath: IndexPath?
    private var dataSource : PostTableViewDataSource<PostTableViewCell,PostInfo>!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the delegates for table view to activate the swipe events in extension
        self.postsTableView.delegate = self
        self.postsTableView.dataSource = self
        
        callToViewModelForUIUpdate()
        configureNavItems()
    }

    private func configureNavItems() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem:.refresh, target: self, action: #selector(handleTabRefreshButton))
    }
    
    @IBAction func touchUpInsideDeleteAll(_ sender: Any) {
        let alert = UIAlertController(title: "Alert", message: "Are you sure you want to delete all posts?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { action in
            switch action.style{
                case .default:
                self.handleMoveAllToTrash()
                case .cancel:
                print("cancel")
                case .destructive:
                print("destructive")
            @unknown default:
                fatalError()
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { action in
            switch action.style{
                case .default:
                print("cancel success")
                case .cancel:
                print("cancel")
                case .destructive:
                print("destructive")
            @unknown default:
                fatalError()
            }
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func callToViewModelForUIUpdate(){
        self.postViewModel =  PostViewModel()
        self.postViewModel.bindPostViewModelToController = {
            self.updateDataSource()
        }
    }
    
    private func updateDataSource(){
        self.dataSource = PostTableViewDataSource(cellIdentifier: "PostTableViewCell", items: self.postViewModel.postsData, configureCell: { (cell, postData) in
            cell.postInfo = postData
        })
        
        DispatchQueue.main.async {
            self.postsTableView.dataSource = self.dataSource
            self.postsTableView.reloadData()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "showDetails") {
            if let vc: PostDetailsViewControllers = segue.destination as? PostDetailsViewControllers {
                guard let tmpSelectedIndexPath =  selectedIndexPath
                else{
                    return;
                }
                vc.setSelectedPostIndex(selectedPost: tmpSelectedIndexPath.row)
                vc.setSelectedPostInfo(selectedPostInfo: postViewModel)
                vc.postActionView.delegate = self;
            }
        }
     }
    
    
    @IBAction func segmentedValueChanged(_ sender: Any) {
        let segment = sender as! UISegmentedControl
        print(segment.selectedSegmentIndex)// 0 , 1
        
        let needFilter = segment.selectedSegmentIndex==1 ? true : false
        self.postViewModel.callFuncToFilterPostData(filter: needFilter)
    }
    
    
    // MARK: - Events
    @objc private func handleTabRefreshButton(){
        callToViewModelForUIUpdate()
    }

    private func handleMoveToTrash(trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) {
        self.postViewModel.callFuncToDeletePostData(at: indexPath.row)
        DispatchQueue.main.async {
            self.postsTableView.reloadData()
        }
    }
    
    private func handleMoveAllToTrash() {
        self.postViewModel.callFuncToDeleteAllPostsData()
        DispatchQueue.main.async {
            self.postsTableView.reloadData()
        }
    }

}
// MARK: - extension
extension PostsViewControllers: PostsViewActionDelegate {
    // Implement the functions required by the delegate
    // protocol definition
    func handleUpdatePosts(postInfo: PostInfo, index: Int) {
        self.postViewModel.callFuncToUpdatePostData(at: index, postInfo: postInfo)
    }
}

/**
 we extend the PostsViewControllers class to listen to the calls of the UITableViewDataSource protocol
 and to be able to modify some characteristics of the table
 */
extension PostsViewControllers: UITableViewDataSource {
    // MARK: - Delegate for extension UITableViewDelegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let countData =  self.postViewModel?.postsData?.count
        else{
            return 0;
        }
        
        return countData
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PostTableViewCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView,
                       trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        // Trash action
        let trash = UIContextualAction(style: .destructive,
                                       title: "Trash") { [weak self] (action, view, completionHandler) in
            self?.handleMoveToTrash(trailingSwipeActionsConfigurationForRowAt: indexPath)
                                        completionHandler(true)
        }
        trash.backgroundColor = .systemRed

        let configuration = UISwipeActionsConfiguration(actions: [trash])

        return configuration
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        self.performSegue(withIdentifier: "showDetails", sender: self)
    }
    
}

extension PostsViewControllers: UITableViewDelegate {
    // ...
}
