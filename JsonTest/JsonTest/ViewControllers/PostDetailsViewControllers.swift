//
//  PostDetailsViewControllers.swift
//  JsonTest
//
//  Created by Carlos Velasquez on 21/07/22.
//

import UIKit

class PostDetailsViewControllers: UIViewController {
    @IBOutlet weak var txtTitlePost: UITextView!
    @IBOutlet weak var txtDescriptionPost: UITextView!
    @IBOutlet weak var lblNameUserPost: UILabel!
    @IBOutlet weak var lblEmailUserPost: UILabel!
    @IBOutlet weak var lblPhoneUserPost: UILabel!
    @IBOutlet weak var lblWebsiteUserPost: UILabel!
    @IBOutlet weak var tableCommentsPost: UITableView!
    private var starButton: UIBarButtonItem?
    private var postDetailsViewModel : PostDetailsViewModel!
    private var postViewModel : PostViewModel!
    private var selectedPostIndex: Int?
    private var dataSource : PostCommentsTableViewDataSource<PostCommentsTableViewCell,PostComment>!
    
    var postActionView = PostsActionView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavStarButton()
        callToViewModelForUIUpdate()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)        
    }
    
    private func configureNavStarButton() {
        starButton = UIBarButtonItem(
            image: UIImage(systemName: "star"),
            style: .plain,
            target: self,
            action: #selector(handleTabFavoriteButton)
        )
        
        self.navigationItem.rightBarButtonItem = starButton
    }
    
    private func configureNavStarFillButton() {
        starButton = UIBarButtonItem(
            image: UIImage(systemName: "star.fill"),
            style: .plain,
            target: self,
            action: #selector(handleTabFavoriteButton)
        )
        
        self.navigationItem.rightBarButtonItem = starButton
    }
    
    func setSelectedPostIndex(selectedPost: Int){
        selectedPostIndex = selectedPost;
    }
    
    func setSelectedPostInfo(selectedPostInfo: PostViewModel){
        self.postViewModel = selectedPostInfo;
    }
    
    func callToViewModelForUIUpdate(){
        guard let tempSelectedPostIndex = selectedPostIndex
        else{
            return;
        }
        
        guard let tempSelectedPostInfo = postViewModel?.postsData[tempSelectedPostIndex]
        else{
            return;
        }
        
        self.postDetailsViewModel =  PostDetailsViewModel(selectedIndexPost: tempSelectedPostIndex, postSelected: tempSelectedPostInfo)
        self.postDetailsViewModel.bindPostDetailsViewModelToControllerOfPostInfo = {
            self.updateDataSourcePostInfo()
        }
        
        self.postDetailsViewModel.bindPostDetailsViewModelToControllerOfUserPost = {
            self.updateDataSourceUserPost()
        }
        
        self.postDetailsViewModel.bindPostCommentsViewModelToControllerOfUserPost = {
            self.updateDataSourcePostComments()
        }
    }
    
    private func updateDataSourcePostInfo(){
        DispatchQueue.main.async {
            self.txtTitlePost.text = self.postDetailsViewModel.postInfo.title
            self.txtDescriptionPost.text = self.postDetailsViewModel.postInfo.body
            
            self.verifyFavorite()
        }
    }
    
    private func updateDataSourceUserPost(){
        DispatchQueue.main.async {
            self.lblNameUserPost.text = self.postDetailsViewModel.userPost.name
            self.lblEmailUserPost.text = self.postDetailsViewModel.userPost.email
            self.lblPhoneUserPost.text = self.postDetailsViewModel.userPost.phone
            self.lblWebsiteUserPost.text = self.postDetailsViewModel.userPost.website
        }
    }
    
    private func updateDataSourcePostComments() {
        self.dataSource = PostCommentsTableViewDataSource(cellIdentifier: "PostCommentsTableViewCell", items: self.postDetailsViewModel.postComments, configureCell: { (cell, postCommentsData) in
            cell.postCommets = postCommentsData
        })
        
        DispatchQueue.main.async {
            self.tableCommentsPost.dataSource = self.dataSource
            self.tableCommentsPost.reloadData()
        }
    }
    
    // MARK: - Events
    @objc private func handleTabFavoriteButton(){
        let isFavorite: Bool = self.postDetailsViewModel.postInfo.favorite ?? false
        self.postDetailsViewModel.setPostToFavorite(isFavorite: !isFavorite)
        verifyFavorite()
        
        guard let tmpSelectedPostIndex =  selectedPostIndex
        else{
            return;
        }
        postActionView.delegate?.handleUpdatePosts(postInfo: self.postDetailsViewModel.postInfo, index: tmpSelectedPostIndex )
    }
    
    private func verifyFavorite(){
        let isFavorite: Bool = self.postDetailsViewModel.postInfo.favorite ?? false
        if (isFavorite) {
            configureNavStarFillButton()
        }else{
            configureNavStarButton()
        }
    }

}
